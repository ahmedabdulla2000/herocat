@extends('admin.layouts.admin-furm')


@section('content')


    <div class="">
        <h2 class="text-monospace float-left m-0">Categories</h2>
        <nav aria-label="breadcrumb" class="float-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                <li class="breadcrumb-item active" aria-current="page">All Category</li>
            </ol>
        </nav>
    </div>
    <a href="{{route('admin.categories.create')}}" class="btn btn-primary float-right mr-3">create</a>
    <table class="table table-bordered table-dark">
        <thead>
        <tr>
            <td>id</td>
            <td>name</td>
            <td>parent</td>
            <td>action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td>
                    @if($category->parent)
                        <a href="{{route('admin.categories.show',$category)}}">{{$category->parent->name}}</a>
                    @endif
                </td>
                <td>
                    <a href="{{route('admin.categories.show',$category)}}" class="btn btn-primary">show</a>

                    <a href="{{route('admin.categories.edit',$category)}}" class="btn btn-danger">edit</a>

                    <a href="{{route('admin.categories.show',$category)}}" class="btn btn-secondary">delete</a>
                </td>
            </tr>

        @endforeach

        </tbody>
    </table>
@stop
