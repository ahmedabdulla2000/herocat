@extends('admin.layouts.admin-furm')
@section('content')

    <div class="">
        <h2 class="text-monospace float-left m-0">Categories</h2>
        <nav aria-label="breadcrumb" class="float-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}">All Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">Show Category: {{$category->name}}</li>
            </ol>
        </nav>
    </div>
<div>
    <div class="clearfix"></div>
    <h1>{{$category->name}}</h1>
    <h1>{{$category->description}}</h1>
    <p>
        @if($category->parent_id)
            {{$category->parent->name}}
        @endif
    </p>
</div>

@stop
