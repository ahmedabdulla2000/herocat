@extends('admin.layouts.admin-furm')
@section('content')
    <div class="">
        <h2 class="text-monospace float-left m-0 ml-4">Create New Category</h2>
        <nav aria-label="breadcrumb" class="float-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}">All Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">create Category</li>
            </ol>
        </nav>
    </div>
<div class="content">
    <div class="container-fluid">
        <form action="{{route('admin.categories.store')}}" method="post">
            @csrf
        @include('admin.layouts.parts.form')
        </form>
    </div>
</div>
@stop
