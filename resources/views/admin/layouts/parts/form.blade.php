
<select class="form-control m-3 ml-0" name="parent_id" id="parent_id">
    <option value="">Not Set</option>

    @foreach($categories as $rowCategory)

@if(isset($cat) && $cat->parent && $rowCategory->id == $cat->parent->id)
        <option value="{{$rowCategory->id}}" selected>{{$rowCategory->name}}</option>
        @endif
        <option value="{{$rowCategory->id}}">{{$rowCategory->name}}</option>
    @endforeach
</select>

{{$errors->first('parent_id')}}
<input type="text" placeholder="name" class="form-control m-3 ml-0" name="name" id="name" value="{{isset($rowCategory) ? $rowCategory->name : ""}}">
<div class="text-danger">
    {{$errors->first('name')}}
</div>
<textarea name="description" id="description" placeholder="description" class="form-control m-3 ml-0" style="height: 140px"></textarea>
<input type="submit" class="form-control m-3" >
