<!DOCTYPE html>
<html lang="en">
<head>
@include('admin.layouts.parts.css')
</head>
<body>
    <div class="wrapper">
                @include('admin.layouts.parts.sidebar')

            <!-- Navbar -->
            @include('admin.layouts.parts.navbar')
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                   @yield('content')
                </div>
            </div>
           @include('admin.layouts.parts.footer')
        </div>
    </div>
</body>
<!--   Core JS Files   -->
@include('admin.layouts.parts.js')

</html>
